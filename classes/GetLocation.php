<?php
/**
 * Created by IntelliJ IDEA.
 * User: jin
 * Date: 11/11/18
 * Time: 4:38 PM
 */

namespace Keios\GeoLocaleSwitcher\Classes;

use Keios\ProUser\Models\Country;

class GetLocation
{

    public static function get()
    {
        // $ip = self::getClientIp();
        $json = file_get_contents("http://ipinfo.io/31.11.130.70/geo");
        $location = json_decode($json, true);
        if (!self::responseValidates($location)) {
            return;
        }

        $country = Country::where('code', $location['country'])->rememberForever(
            'country_'.$location['country']
        )->first();

        $cc = new CallingCodes();
        $prefix = $cc->get($location['country']);
        $location['iso_code'] = $location['country'];
        $location['country'] = $country->name;
        $location['countryModel'] = $country;
        if (array_key_exists(0, $prefix)) {
            $location['prefix'] = '+'.$prefix[0];
        }

        return $location;
    }

    public static function getClientIp()
    {
        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED'])) {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        } elseif (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_FORWARDED'])) {
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        } else {
            $ipaddress = 'UNKNOWN';
        }

        return $ipaddress;
    }

    private static function responseValidates($location)
    {
        if (!array_key_exists('country', $location)) {
            return false;
        }

        return true;
    }
}
