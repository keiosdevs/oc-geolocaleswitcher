<?php namespace Keios\Geolocaleswitcher\Components;

use Keios\GeoLocaleSwitcher\Classes\GeoLocaleRepository;
use RainLab\Translate\Models\Locale as LocaleModel;
use RainLab\Translate\Classes\Translator;
use RainLab\Translate\Models\Locale;
use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use App;
use Session;

/**
 * Class LocaleAsk
 *
 * @package Keios\Geolocaleswitcher\Components
 */
class LocaleAsk extends ComponentBase
{

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'keios.geolocaleswitcher::lang.localecomponent.title',
            'description' => 'keios.geolocaleswitcher::lang.localecomponent.description',
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getLocaleChangePageOptions()
    {
        return ['' => '- none -'] + Page::sortBy('baseFileName')->lists('baseFileName', 'url');
    }

    /**
     * onRun - assign global twig variables
     */
    public function onRun()
    {
        $location = $this->detectedLocation();
        $currentLocale = Translator::instance()->getLocale();
        $displayedLocales = $this->displayedLocales($location['iso']);
        $this->page['detected_location'] = $location;
        $this->page['detected_locales'] = $displayedLocales;
        $displayedLanguagesIsos = array_keys($displayedLocales);
        if (\count($displayedLanguagesIsos) > 0) {
            /** @noinspection NestedPositiveIfStatementsInspection */
            if (\count($displayedLanguagesIsos) > 1 || $currentLocale !== $displayedLanguagesIsos[0]) {
                $this->page['show_localeask'] = true;
            }
        }

    }

    /**
     * @return null
     */
    public function localeChangePage()
    {
        $localeChangePage = $this->property('localeChangePage');

        return ($localeChangePage ?: null);
    }

    /**
     * @return array
     */
    public function detectedLocation()
    {
        $location = @unserialize (file_get_contents('http://ip-api.com/php/'));
        $country = null;
        $city = null;
        $iso = null;
        $country = $location['country'];
        $city = $location['city'];
        $iso = $location['countryCode'];

        return [
            'country' => $country,
            'city'    => $city,
            'iso'     => $iso,
        ];
    }

    /**
     * @return mixed
     */
    public function currentLocale()
    {
        $currentLocale = Translator::instance()->getLocale();
        $allAvailableLocale = Locale::listAvailable();

        return $allAvailableLocale[$currentLocale];
    }

    /**
     * @param string|null $locationCode
     * @return array
     */
    public function displayedLocales($locationCode = null)
    {
        $availableLocales = LocaleModel::listAvailable();
        $repo = new GeoLocaleRepository();
        if (!$locationCode) {
            $locationCode = \Session::get('geoip-location.isoCode');
        }
        /** @var array $countryLocales */
        $countryLocales = $repo->resolve($locationCode)->getAllLocales();
        $availableLocaleIds = [];
        $displayedLocales = [];
        foreach ($availableLocales as $localeId => $locale) {
            $availableLocaleIds[] = $localeId;
        }

        foreach ($countryLocales as $locale) {
            if (\in_array($locale, $availableLocaleIds, false)) {
                $localeName = $availableLocales[$locale];
                $displayedLocales[$locale] = $localeName;
            }
        }

        return $displayedLocales;
    }

}
